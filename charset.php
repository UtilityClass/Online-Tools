<?php
if (! function_exists ( 'hex2bin' )){
  function hex2bin($hexstr) 
  { 
      $n = strlen($hexstr); 
      $sbin="";   
      $i=0; 
      while($i<$n) 
      {       
          $a =substr($hexstr,$i,2);           
          $c = pack("H*",$a); 
          if ($i==0){$sbin=$c;} 
          else {$sbin.=$c;} 
          $i+=2; 
      } 
      return $sbin; 
  }
}
if (strtoupper($_REQUEST['charset']) == 'GBK'){
  echo  bin2hex(@iconv("UTF-8", "GBK//IGNORE", hex2bin($_REQUEST['data'])));
}else{
  echo  bin2hex(@iconv("GBK", "UTF-8//IGNORE", hex2bin($_REQUEST['data'])));
}